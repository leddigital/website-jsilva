<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Informations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'address1',
        'number1',
        'complement1',
        'district1',
        'zipcode1',
        'city1',
        'state1',
        'address2',
        'number2',
        'complement2',
        'district2',
        'zipcode2',
        'city2',
        'state2',
        'whatsapp',
        'instagram',
        'facebook',
        'linkein',
        'twitter',
        'pinterest',
        'email1',
        'email2',
        'phone1',
        'phone2',
        'main_banner',
        'fact_1',
        'fact_2',
        'fact_3',
        'fact_1_text',
        'fact_2_text',
        'fact_3_text',
        'fact_1_icon',
        'fact_2_icon',
        'fact_3_icon',
        'meta_description'
    ];
}
