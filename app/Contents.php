<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contents extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'image',
        'tags',
        'tags_min',
        'type',
        'url'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];

    public function images(){
        return $this->hasMany('App\ContentsImages');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_contents');
    }
}
