<?php

namespace App\Http\Controllers;

use App\Banners;
use App\Contents;
use App\Informations;
use Illuminate\Http\Request;


class NavigationController extends Controller
{
    public function index(Request $request)
    {
        $banners = Banners::all();
        $news = Contents::where('type', '=', 'news')->orderBy('created_at', 'desc')->limit(3)->get();
        $projetos = Contents::where('type', '=', 'projetos')->get()->all();
        $informations = Informations::first();

        return view('pages.index',
            ['banners' => $banners, 'news' => $news, 'projetos' => $projetos, 'informations' => $informations]);
    }

    public function about(Request $request)
    {
        $informations = Informations::get()->first();
        $content = Contents::where('type', '=', 'sobre')->get()->last();

        return view('pages.sobre',
            ['informations' => $informations, 'content' => $content]);
    }

    public function services(Request $request)
    {
        $informations = Informations::get()->first();
        return view('pages.services');
    }

    public function midias(Request $request) {

        $informations = Informations::get()->first();
        $midias = Contents::where('type', '=', 'midias')->get()->all();
        return view('pages.midias',
            ['midias' => $midias, 'informations' => $informations]);

    }

    public function midia(Request $request) {

        $url = $request->route('midia');
        $informations = Informations::get()->first();
        $content = new Contents();
        $midia = $content->with('images')
        ->where('url', '=', $url)
        ->get()->first();

        $midias = Contents::where('type', '=', 'midias')->get()->all();

        // Primeira imagem da mídia que será usada na meta tag do Facebook
        $single_image = $midia['images'][0]->image;

        return view('pages.midia',
            [
                'midia' => $midia,
                'midias' => $midias,
                'informations' => $informations,
                'single_image' => $single_image
            ]
        );
    }

    public function search(Request $request) {

        $informations = Informations::get()->first();

        $form = $request->all();
        $posts = Contents::Where('type', '=', 'news')
        ->Where('title', 'like', '%'.$form['search'].'%')
        ->paginate(6);

        return view('pages.blog',
            ['posts' => $posts,  'informations' => $informations]);
    }

    public function tags (Request $request) {

        $form = $request->all();

        $entity = new Contents();
        $posts = $entity->where('type', '=', 'news')->get()->all();

        $informations = Informations::get()->first();
        // echo '<h1>'.$form['tag'].'</h1>';

        echo "<pre>";
        foreach ($posts as $post) {
            print_r($post['tags_min']);
        }
        echo "</pre>";

    }

    public function blog(Request $request)
    {
        $informations = Informations::get()->first();

        $posts = Contents::where('type', '=', 'news')->paginate(8);
        return view('pages.blog',
            ['posts' => $posts, 'informations' => $informations]);
    }

    public function post(Request $request)
    {
        $informations = Informations::get()->first();

        $url = $request->route('url');
        $post = Contents::where('url', '=', $url)->get()->first();

        $latest_post = Contents::orderBy('created_at', 'desc')
        ->where('url', '<>', $url)
        ->where('type', '=', 'news')
        ->take(4)->get();

        $midias = Contents::where('type', '=', 'midias')->get()->all();

        //$tags = explode(",", $this->url_verify($post['tags'], $this->model));
        return view('pages.post',
            ['post' => $post, 'latests' => $latest_post, 'informations' => $informations, 'midias' => $midias]);
    }

    public function contact(Request $request)
    {
        $informations = Informations::get()->first();
        return view('pages.contato', ['informations' => $informations]);
    }

}
