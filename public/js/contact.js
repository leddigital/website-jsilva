$(document).ready(function(){

    (function($) {
        "use strict";


    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                subject: {
                    required: true,
                    minlength: 4
                },
                number: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 20
                }
            },
            messages: {
                name: {
                    required: "Por favor informe seu nome",
                    minlength: "Seu nome precisa conter ao menos 2 caracteres"
                },
                subject: {
                    required: "Por favor informe o assunto da sua mensagem",
                    minlength: "Seu assunto precisa conter ao menos 4 caracteres"
                },
                number: {
                    required: "Por favor inform seu número de telefone para contato",
                    minlength: "Seu número de telefone deve conter ao menos 8 caracteres"
                },
                email: {
                    required: "Por favor informe seu e-mail"
                },
                message: {
                    required: "Sua mensagem não pode ser vazia",
                    minlength: "Sua mensagem não pode ter menos de 20 caracteres"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    // data: $(form).serialize(),
                    url: $(form).attr('action'),
                    success: function() {
                        $('#contactForm :input').attr('disabled', 'disabled');
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                	$('#success').modal('show');
                        })
                    },
                    error: function() {
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                	$('#error').modal('show');
                        })
                    }
                })
            }
        })
    })

 })(jQuery)
})


$(function () {
    $('.personalmask').attr('maxlength', '18');
    $('.phonemask').attr('maxlength', '15');
    $('.cepmask').attr('maxlength', '9');
    $('.onlynumber').keypress(function () {
        var code = (event.keyCode ? event.keyCode : event.which);
        if ((code > 47 && code < 58))
            return true;
        else {
            if (code == 8 || code == 0)
                return true;
            else
                return false;
        }
    });

    /* Aplicando mascara de CPF/CNPJ */
    $('.personalmask').keyup(function () {
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if (v.length <= 13) { //CPF
            v = v.replace(/(\d{3})(\d)/, "$1.$2");
            v = v.replace(/(\d{3})(\d)/, "$1.$2");
            v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            $(this).val(v);
        } else { //CNPJ
            v = v.replace(/^(\d{2})(\d)/, "$1.$2");
            v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
            v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
            v = v.replace(/(\d{4})(\d)/, "$1-$2");
            $(this).val(v);
        }
    });

    /* Aplicando Mascara de Telefones/Celular */
    $('.phonemask').keyup(function () {
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if ($(this).val().length <= 10) {
            v = v.replace(/^(\d{2})(\d)/, "($1) $2");
            v = v.replace(/(\d{4})(\d)/, "$1-$2");
            $(this).val(v);
        } else {
            v = v.replace(/^(\d{2})(\d)/, "($1) $2");
            v = v.replace(/(\d{5})(\d)/, "$1-$2");
            $(this).val(v);
        }
    });

    /* Aplicando mascara de CEP */
    $('.cepmask').keyup(function () {
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if ($(this).val().length <= 8) {
            v = v.replace(/^(\d{5})(\d)/, "$1-$2");
            $(this).val(v);
        }
    });

    $('.datemask').keyup(function () {
        $(this).attr('maxlength', '10');
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if ($(this).val().length <= 10) {
            v = v.replace(/^(\d{2})(\d)/, "$1/$2");
            v = v.replace(/(\d{2})(\d)/, "$1/$2");
            $(this).val(v);
            /*Validade se data é valida*/
        }
    });

    $('.datemask').on('blur', function () {
        var regex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
        var txt = $(this).val();
        if (!regex.test(txt)) {
            $(this).val('');
            $(this).focus();
        }
    })

});
