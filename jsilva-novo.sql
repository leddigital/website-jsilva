-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: jsilva_site
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text,
  `info2` text,
  `info3` text,
  `link1` text,
  `link2` text,
  `link3` text,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (11,'banner_5dc5b25541cff.jpeg','Banner 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-08 18:22:13','2019-11-08 18:22:13',NULL,NULL),(15,'banner_5dc5b2bad4cfb.jpeg','Banner 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-08 18:23:54','2019-11-08 18:23:54',NULL,NULL),(16,'banner_5dc5b2dc979ed.jpeg','Banner 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-08 18:24:28','2019-11-08 18:24:28',NULL,NULL);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (4,'Blog','blog','news'),(12,'Projetos Especiais','projetos-especiais','projeto'),(13,'Mídias','midias','midias'),(17,'Sobre','sobre','produtos'),(18,'Valores','valores','valores');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_has_contents`
--

DROP TABLE IF EXISTS `categories_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`categories_id`,`contents_id`),
  KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  KEY `fk_categories_has_contents_categories1_idx` (`categories_id`),
  CONSTRAINT `fk_categories_has_contents_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_has_contents`
--

LOCK TABLES `categories_has_contents` WRITE;
/*!40000 ALTER TABLE `categories_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `short_description` text,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `tags` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tags_min` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (27,'Projeto Especial 1',NULL,NULL,NULL,'projetos','projeto-especial-1','15713199245da87074e455a.jpeg',NULL,'2019-10-17 13:45:24','2019-12-10 12:14:59',NULL),(28,'Projeto Especial 2',NULL,NULL,NULL,'projetos','projeto-especial-2','15713199645da8709c6336f.jpeg',NULL,'2019-10-17 13:46:04','2019-12-10 12:14:49',NULL),(29,'Projeto Especial 3',NULL,NULL,NULL,'projetos','projeto-especial-3','15713200075da870c725748.jpeg',NULL,'2019-10-17 13:46:47','2019-12-10 12:14:42',NULL),(34,'Outdoor',NULL,'O Outdoor destaca sua marca e impacta o público que passa pelo ponto onde está inserido. Presente no cotidiano das pessoas, faz parte da paisagem urbana. Não há como ficar indiferente. Outdoor é uma mídia rápida e eficiente.','<p>O Outdoor destaca sua marca e impacta o público que passa pelo ponto onde está inserido. Presente no cotidiano das pessoas, faz parte da paisagem urbana. Não há como ficar indiferente. Outdoor é uma mídia rápida e eficiente.</p><p>A localização dos pontos proporciona cobertura, alcance e frequência na exposição das mensagens. E aliado a um bom planejamento, o outdoor permite a criação de campanhas tanto massivas, quanto segmentadas para um público-alvo.</p><p>COM UMA SELEÇÃO DE 15 LOCAIS EM SÃO JOSÉ DO RIO PRETO, É POSSÍVEL ATINGIR, EM UMA BISSEMANA:</p><p>Impacto: 2,6 milhões de pessoas</p><p>Audiência: 634 mil pessoas</p><p><b>QUANTIDADE INDICADA</b></p><p>Para uma boa cobertura em São José do Rio Preto, é indicado a compra de 10 a 15 locais.</p><p>COBERTURA: São José do Rio Preto, Bady Bassitt, Bálsamo, Fronteira, Ibirá, Jaci, Mirrasol, Monte Aprazível, Neves Paulista, Nova Granada e Potirendaba.</p><p>Formato: 3 m x 9 m = 27 m² </p><p>Comercialização: Bissemana</p><p> Produção: papel ou lona</p><p><b><span style=\"font-size: 14px;\">Outdoor Sequencial</span></b></p><p>O uso adequado do Outdoor Sequencial gera impacto e aumenta o tempo de exposição da sua marca aos olhos do consumidor. Instalados em lugares de grande circulação e visibilidade, são bem percebidos e reforçam a mensagem veiculada.</p><p>COLOCANDO O PINGO NO I.&nbsp;</p><p>Para nós, é importante que você entenda tudo para comprovar o bom investimento que é a Mídia Exterior.&nbsp;</p><p>O que significa</p><p>Audiência: é o total de pessoas atingidas pela mídia, pela mensagem passada pela sua marca.&nbsp;</p><p>Impacto: total (bruto) de pessoas que tiveram contato e/ou passaram pela mídia ao longo da duração da mensagem.&nbsp;</p>','midias','outdoor','15732431775dc5c92900d1c.jpeg',NULL,'2019-10-22 18:11:00','2019-11-12 20:47:15',NULL),(35,'Painel Rodoviário',NULL,'Podem ser facilmente vistos nas principais rodovias do país, tanto pelos formatos, quanto pela visibilidade.','<p>Podem ser facilmente vistos nas principais rodovias do país, tanto pelos formatos, quanto pela visibilidade. Uma mídia que atinge milhões de pessoas por mês.</p><p>Ainda gera impacto e recordação, por meio de uma excelente estrutura e alta definição na impressão.</p><p>COM APENAS 7 PAINÉIS NAS PRINCIPAIS RODOVIAS DE SÃO PAULO, É POSSÍVEL ALCANÇAR:</p><p>Impacto: 6.500.000 de pessoas.&nbsp;</p><p>Audiência: 3.000.000 de pessoas.</p><p>*fonte: ícone everywhere analitics</p><p>Cobertura: Estado de São Paulo </p><p> Formato: sob consulta </p><p>Comercialização: mensal</p><p> Produção: lona</p><p>COLOCANDO O PINGO NO I.&nbsp;</p><p>Para nós, é importante que você entenda tudo para comprovar o bom investimento que é a Mídia Exterior. <br></p><p>O que significa</p><p>Audiência: é o total de pessoas atingidas pela mídia, pela mensagem passada pela sua marca.&nbsp;</p><p>Impacto: total (bruto) de pessoas que tiveram contato e/ou passaram pela mídia ao longo da duração da mensagem. <br></p>','midias','painel-rodoviario','15732439165dc5cc0c184d7.jpeg',NULL,'2019-10-22 19:10:23','2019-11-12 20:48:36',NULL),(36,'Frontlight',NULL,'Os painéis iluminados garantem visibilidade dia e noite e geram grande impacto para o anunciante, sem interrupção.','<p>Os painéis iluminados garantem visibilidade dia e noite e geram grande impacto para o anunciante, sem interrupção. Sua marca estará presente nas principais avenidas de São José do Rio Preto. Investir em frontlights é divulgar o seu negócio 24h por dia com eficiência.</p><p>COM UMA SELEÇÃO DE 3 LOCAIS É POSSÍVEL ATINGIR:</p><p>Impacto: 1,2 milhões de pessoas/mês</p><p>Audiência: 395 mil pessoas/mês</p><p>Iluminação: frontlight</p><p> Formato: sob consulta </p><p>Comercialização: mensal </p><p>Produção: lona</p><p>COLOCANDO O PINGO NO I.&nbsp;</p><p>Para nós, é importante que você entenda tudo para comprovar o bom investimento que é a Mídia Exterior. <br></p><p>O que significa</p><p>Audiência: é o total de pessoas atingidas pela mídia, pela mensagem passada pela sua marca.&nbsp;</p><p>Impacto: total (bruto) de pessoas que tiveram contato e/ou passaram pela mídia ao longo da duração da mensagem. <br></p>','midias','frontlight','15732437825dc5cb86e973b.jpeg',NULL,'2019-10-22 19:13:37','2019-11-12 20:49:37',NULL),(37,'Empena',NULL,'Agigante ainda mais sua marca em um formato que chama a atenção pela grandiosidade.','<p>Agigante ainda mais sua marca em um formato que chama a atenção pela grandiosidade. As empenas são instaladas em laterais de prédios e geram visibilidade e impacto para quem transita pelas regiões próximas aos pontos. <br></p><p>É POSSÍVEL ATINGIR EM SEIS MESES, UMA MÉDIA DE:</p><p>Impacto: 346.463 pessoas/mês</p><p>Audiência: 199.140 pessoas/mês</p><p>Formato:  10,45 m x 22 m: 230 m²</p><p> Comercialização: semestral</p><p> Produção: lona</p><p>COLOCANDO O PINGO NO I.&nbsp;</p><p>Para nós, é importante que você entenda tudo para comprovar o bom investimento que é a Mídia Exterior. <br></p><p>O que significa</p><p>Audiência: é o total de pessoas atingidas pela mídia, pela mensagem passada pela sua marca.&nbsp;</p><p>Impacto: total (bruto) de pessoas que tiveram contato e/ou passaram pela mídia ao longo da duração da mensagem. <br></p>','midias','empena','15732438625dc5cbd67eb3c.jpeg',NULL,'2019-10-22 19:14:32','2019-11-12 20:50:47',NULL),(38,'Relógio',NULL,'Pra quem sabe que deixar de anunciar é perder tempo, os relógios digitais são estrategicamente distribuídos pelas cidades, captando a atenção do público por mostrar horário e temperatura.','<p>Pra quem sabe que deixar de anunciar é perder tempo, os relógios digitais são estrategicamente distribuídos pelas cidades, captando a atenção do público por mostrar horário e temperatura. Cobertura nas cidades de São José do Rio Preto, Votuporanga, Olímpia e Barretos.</p><p>UTILIDADE PÚBLICA E COMUNICAÇÃO</p><p>COM UMA SELEÇÃO DE 10 LOCAIS EM SÃO JOSÉ DO RIO PRETO, É POSSÍVEL IMPACTAR: 2.000.000 de pessoas*</p><p>*fonte – everywhere analytics</p><p>Iluminação: Backlight </p><p>Comercialização: Bissemana </p><p>Formato peça: 2,02 m x 1,40 m</p><p>Área Visual: 1,80 m x1,20 m </p><p>Exposição: 3 faces</p><p> Tempo de exposição: 8 segundos cada | 3 vezes por minuto | 180 vezes por hora | 4.320 vezes por dia (24h) | 60.480 vezes por bissemana</p><p>COLOCANDO O PINGO NO I.&nbsp;</p><p>Para nós, é importante que você entenda tudo para comprovar o bom investimento que é a Mídia Exterior. <br></p><p>O que significa</p><p>Audiência: é o total de pessoas atingidas pela mídia, pela mensagem passada pela sua marca.&nbsp;</p><p>Impacto: total (bruto) de pessoas que tiveram contato e/ou passaram pela mídia ao longo da duração da mensagem. <br></p>','midias','relogio','15732435865dc5cac259562.jpeg',NULL,'2019-10-22 19:15:10','2019-11-12 20:57:59',NULL),(39,'Ático',NULL,'Mantenha sua marca sempre no topo, com localização privilegiada!','<p>Mantenha sua marca sempre no topo, com localização privilegiada! Os áticos podem ser vistos de diversos pontos da cidade, dia e noite, inclusive por quem passa pelas principais rodovias: Whashington Luis, Assis Chateaubriand e BR – 153.</p><p>Proporciona visibilidade e até participação especial em cartões postais da cidade.</p><p>Iluminação: frontlight  </p><p>Formato: sob consulta</p><p> Comercialização: semestral ou anual </p><p>Produção: lona</p><p><br></p>','midias','atico','15732434795dc5ca57bf17e.jpeg',NULL,'2019-10-22 19:16:11','2019-11-12 21:03:07',NULL),(40,'NOSSA HISTÓRIA',NULL,'OFERECER O MELHOR SERVIÇO PARA OS NOSSOS CLIENTES','<p>Desde 1971 nosso ponto principal é o de evidenciar a marca e contribuir para o sucesso dos nossos clientes. Assim, nos tornamos referência e líderes no mercado de Mídia Exterior. Nosso papel é ser diferente e diferenciar a sua marca, conversando e entendendo sobre cada necessidade, pensando em soluções pontuais e eficientes.</p><p>Consideramos a Mídia Exterior um meio plural, diversificado e de grande retorno para quem investe. Em cada uma das possibilidades de anúncio é possível impactar um público determinado, isso faz com que você possa, ao mesmo tempo, atingir o seu cliente e chegar até outros potenciais.&nbsp;&nbsp;</p><p>Sabemos o que fazemos. Trabalhamos com a mídia que vem se consolidando a cada ano entre as preferidas dos anunciantes. Acompanhamos as tendências, as realidades e as levamos até a sua marca, para que você venda mais, apareça mais, evolua mais.&nbsp;</p><p>Conheça o nosso portfólio de sucesso. Ele combina muito bem com o seu negócio!</p>','sobre','nossa-historia','15732467555dc5d72313adc.jpeg',NULL,'2019-10-24 14:00:10','2019-11-08 21:00:10',NULL);
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_contents`
--

DROP TABLE IF EXISTS `contents_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`,`contents_child_id`),
  KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  KEY `fk_contents_has_contents_contents1_idx` (`contents_id`),
  CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_contents`
--

LOCK TABLES `contents_has_contents` WRITE;
/*!40000 ALTER TABLE `contents_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_images`
--

DROP TABLE IF EXISTS `contents_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text,
  PRIMARY KEY (`id`,`contents_id`),
  KEY `fk_contents_images_contents_idx` (`contents_id`),
  CONSTRAINT `fk_contents_images_contents` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_images`
--

LOCK TABLES `contents_images` WRITE;
/*!40000 ALTER TABLE `contents_images` DISABLE KEYS */;
INSERT INTO `contents_images` VALUES (25,NULL,'gallery','0','15732433965dc5ca04b0057.jpeg',34,'/home2/agenci62/jsilva.agencialed.com.br/public/content//34/'),(26,NULL,'gallery','0','15732433965dc5ca04b1a20.jpeg',34,'/home2/agenci62/jsilva.agencialed.com.br/public/content//34/'),(27,NULL,'gallery','0','15732433965dc5ca04b3be3.jpeg',34,'/home2/agenci62/jsilva.agencialed.com.br/public/content//34/'),(28,NULL,'gallery','0','15732434795dc5ca57bfbd7.jpeg',39,'/home2/agenci62/jsilva.agencialed.com.br/public/content//39/'),(29,NULL,'gallery','0','15732434795dc5ca57c0d85.jpeg',39,'/home2/agenci62/jsilva.agencialed.com.br/public/content//39/'),(30,NULL,'gallery','0','15732435865dc5cac25a51a.jpeg',38,'/home2/agenci62/jsilva.agencialed.com.br/public/content//38/'),(31,NULL,'gallery','0','15732435865dc5cac25ba9a.jpeg',38,'/home2/agenci62/jsilva.agencialed.com.br/public/content//38/'),(32,NULL,'gallery','0','15732437825dc5cb86ea397.jpeg',36,'/home2/agenci62/jsilva.agencialed.com.br/public/content//36/'),(33,NULL,'gallery','0','15732437825dc5cb86ebd83.jpeg',36,'/home2/agenci62/jsilva.agencialed.com.br/public/content//36/'),(34,NULL,'gallery','0','15732438625dc5cbd67f7b9.jpeg',37,'/home2/agenci62/jsilva.agencialed.com.br/public/content//37/'),(35,NULL,'gallery','0','15732439165dc5cc0c1957e.jpeg',35,'/home2/agenci62/jsilva.agencialed.com.br/public/content//35/');
/*!40000 ALTER TABLE `contents_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address1` text,
  `number1` varchar(45) DEFAULT NULL,
  `complement1` varchar(255) DEFAULT NULL,
  `district1` varchar(255) DEFAULT NULL,
  `zipcode1` varchar(45) DEFAULT NULL,
  `city1` varchar(255) DEFAULT NULL,
  `state1` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `email1` text,
  `email2` text,
  `address2` text,
  `number2` varchar(45) DEFAULT NULL,
  `complement2` varchar(255) DEFAULT NULL,
  `district2` varchar(255) DEFAULT NULL,
  `zipcode2` varchar(45) DEFAULT NULL,
  `city2` varchar(255) DEFAULT NULL,
  `state2` varchar(255) DEFAULT NULL,
  `main_banner` text,
  `fact_1` varchar(255) DEFAULT NULL,
  `fact_2` varchar(255) DEFAULT NULL,
  `fact_3` varchar(255) DEFAULT NULL,
  `fact_1_text` varchar(255) DEFAULT NULL,
  `fact_2_text` varchar(255) DEFAULT NULL,
  `fact_3_text` varchar(255) DEFAULT NULL,
  `fact_1_icon` varchar(2) DEFAULT NULL,
  `fact_2_icon` varchar(2) DEFAULT NULL,
  `fact_3_icon` varchar(2) DEFAULT NULL,
  `meta_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Av. Mauricio Costa','30',NULL,'Centro','15895-000','Cedral','SP','(17) 99727-6993','https://www.instagram.com/jsilvapaineis/','https://www.facebook.com/jsilvapaineis/',NULL,NULL,NULL,'(17) 3266-9393','(17) 2138-8969','contato@jsilvapaineis.com.br','suporte@jsilvaoutdoor.com.br','Av. Fernando Costa','137',NULL,'Maceno','15061-000','S.J. Rio Preto','SP','mainbanner_5db34c0bb4e19.jpeg','3','80','60','MÍDIA + ESCOLHIDA PELOS ANUNCIANTES','DE IMPACTO','DOS JOVENS CONFIAM NA MÍDIA','º','%','%','Desde 1971 nosso ponto principal é o de evidenciar a marca e contribuir para o sucesso dos nossos clientes. Assim, nos tornamos referência e líderes no mercado de Mídia Exterior. Nosso papel é ser diferente e diferenciar a sua marca, conversando e entendendo sobre cada necessidade, pensando em soluções pontuais e eficientes.');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'leddigital','digital@agencialed.com.br',NULL,'$2y$10$6NsTVvIenPGcOpcmLpjxIeS5p5Q4oTK59ThKMj9kuCnRgIwisgUgq','fRxlJZDRSNlkoUzwym7xsJ9XUQvn5Qe9E5x951FoO5Wlqxy61qpoTYDWuXp8','2019-09-26 20:22:30','2019-09-26 20:22:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'jsilva_site'
--

--
-- Dumping routines for database 'jsilva_site'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-11 15:23:06
