<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <title>JSilva Painéis</title>
    <meta name="description" content="{{ $informations->meta_description }}" />
    <meta name="author" content="JSilva Paineis">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @yield('social-tags')

    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Icon css link -->
    <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/stroke-icon/style.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/flat-icon/flaticon.css')}}" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="{{ asset('vendors/revolution/css/settings.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/revolution/css/layers.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/revolution/css/navigation.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/animate-css/animate.css')}}" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="{{ asset('vendors/magnify-popup/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/owl-carousel/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/bootstrap-selector/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/lightbox/simpleLightbox.css')}}" rel="stylesheet">

    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

    <!-- Custom style.css-->
    <link href="{{ asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css')}}" rel="stylesheet">

    @yield('css')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
<body>

    <!--================Header Area =================-->
    <header class="main_header_area">
        <div class="header_top">
            <div class="container">
                <div class="header_top_inner">
                    <div class="pull-left">
                        <a href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone1)?>"><i class="fa fa-phone"></i>{{ $informations->phone1 }}</a>
                        <a href="mailto:{{ $informations->email1 }}" target="_blank"><i class="fa fa-envelope-o"></i>{{ $informations->email1 }}</a>
                        <a href="{{ $informations->instagram }}" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="{{ $informations->facebook }}" target="_blank"><i class="fa fa-facebook"></i></i></a>
                        <a href="https://wa.me/+55<?= preg_replace('/[^0-9]+/','', $informations->whatsapp)?>" target="_blank">Fale Conosco<i class="fa fa-whatsapp"></i></a>
                    </div>
                    <div class="pull-right">
                        <ul class="header_social">
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <a href="http://newesouth.logycware.com.br/demonstracao/" target="_blank">Mapa dos pontos</a></li>
                            {{-- <li><a href="index.html#">Área do anunciante</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header_menu">
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('nav.index') }}">
                            <img src="{{ asset('images/logo.png') }}" alt="Logo JSilva">
                            <img src="{{ asset('images/logo.png') }}" alt="Logo JSilva">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="{{ Request::is('/') ? 'nav-link active' : '' }}"><a href="{{ route('nav.index') }}">Home</a></li>
                            <li class="{{ Request::is('sobre') ? 'nav-link active' : '' }}"><a href="{{ route('nav.about') }}">J.Silva</a></li>
                            <li class="{{ Request::is('midias') ? 'nav-link active' : '' }}"><a href="{{ route('nav.midias') }}">O.O.H.</a></li>
                            <li class="{{ Request::is('blog') ? 'nav-link active' : '' }}"><a href="{{ route('nav.blog') }}">Notícias</a></li>
                            <li class="{{ Request::is('contato') ? 'nav-link active' : '' }}"><a href="{{ route('nav.contact') }}">Contato</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </header>

    @yield('content')

    <!--================Footer Area =================-->
    <footer class="footer_area">
        <div class="footer_widget_area">
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-md-3 col-xs-6 image-footer h-100">
                        <div class="row h-100">
                            <div class="col-sm-12">
                                <img src="{{ asset('images/logo-rodape.png') }}" alt="">
                            </div>
                        </div>



                    </div>
                    <div class="col-md-3 col-xs-6">
                        <aside class="f_widget about_widget">
                            <div class="list-wrapper">
                                <ul class="list-unstyled">
                                    <li class="list-unstyled-item">
                                        <h2 class="footer-title">J. Silva Painéis</h2>
                                    </li>
                                    <li class="list-unstyled-item">
                                        <i class="fa fa-map-marker footer-icon" aria-hidden="true"></i>
                                        <h5 class="footer-text">{{ $informations->address1 }}, {{ $informations->number1 }}, {{ $informations->district1 }} <br /> {{ $informations->city1 }} - {{ $informations->state1 }} {{ $informations->zipcode1 }}</h5>
                                    </li>
                                    <li class="list-unstyled-item">
                                        <i class="fa fa-phone footer-icon" aria-hidden="true"></i>
                                        <h5 class="footer-text">{{ $informations->phone1 }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <aside class="f_widget about_widget">
                            <div class="list-wrapper">
                                <ul class="list-unstyled">
                                    <li class="list-unstyled-item">
                                        <h2 class="footer-title">J. Silva Outdoor</h2>
                                    </li>
                                    <li class="list-unstyled-item">
                                        <i class="fa fa-map-marker footer-icon" aria-hidden="true"></i>
                                        <h5 class="footer-text">{{ $informations->address2 }}, {{ $informations->number2 }}, {{ $informations->district2 }} <br /> {{ $informations->city2 }} - {{ $informations->state2 }} {{ $informations->zipcode2 }}</h5>
                                    </li>
                                    <li class="list-unstyled-item">
                                        <i class="fa fa-phone footer-icon" aria-hidden="true"></i>
                                        <h5 class="footer-text">{{ $informations->phone2 }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <aside class="f_widget about_widget">
                            <div class="list-wrapper">
                                <ul class="list-unstyled">
                                    <li class="list-unstyled-item">
                                        <h2 class="footer-title t-upper">Horário de funcionamento</h2>
                                    </li>
                                    <li class="list-unstyled-item">
                                        <h5 class="footer-text">Segunda a sexta<br />das 8h às 18h</h5>
                                    </li>
                                </ul>
                            </div>
                            {{-- <div class="list-wrapper">
                                <h2 class="footer-title t-upper t_inverse">Mapa do site</h2>
                                <hr class="footer-separator">
                                <ul class="footer-list">
                                    <li><a href="javascript:;">Pontos</a></li>
                                    <li><a href="javascript:;">Mídia Exterior</a></li>
                                    <li><a href="javascript:;">Blog</a></li>
                                    <li><a href="javascript:;">Projetos Especiais</a></li>
                                    <li><a href="javascript:;">Orçamento</a></li>
                                    <li><a href="javascript:;">Clientes</a></li>
                                    <li><a href="javascript:;">Portfólio</a></li>
                                    <li><a href="javascript:;">Mapa dos Pontos</a></li>
                                </ul>
                            </div> --}}
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_copyright_area">
            <div class="container">
                <div class="atribuition-text">
                    <h4>&copy; Grupo J.Silva {{ now()->year }} | Todos os direitos reservados.</h4>
                </div>
                <div class="atribuition-text">
                    <h4>Desenvolvido por <a href="http://leddigital.com.br" target="_blank">LED Digital</a></h4>
                </div>
            </div>
        </div>
    </footer>
    <!--================End Footer Area =================-->

    <!--================Search Box Area =================-->
    <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
        <div class="search_box_inner">
            <h3>Search</h3>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
                </span>
            </div>
        </div>
    </div>
    <!--================End Search Box Area =================-->





    <!--================End Footer Area =================-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('js/jquery-2.2.4.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <!-- Rev slider js -->
    <script src="{{ asset('vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{ asset('vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
    <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{ asset('vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>

    <script src="{{ asset('vendors/magnify-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('vendors/isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('vendors/counterup/waypoints.min.js')}}"></script>
    <script src="{{ asset('vendors/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{ asset('vendors/bootstrap-selector/bootstrap-select.js')}}"></script>
    <!--<script src="vendors/lightbox/js/lightbox.min.js"></script>-->
    <script src="{{ asset('vendors/lightbox/simpleLightbox.min.js')}}"></script>

    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsplfQWqFB0g3ITe5rhhi-V1QeOBhJh_8"></script>
    <script src="{{ asset('js/gmaps.min.js') }}"></script>

    <!-- contact js -->
    <script src="{{ asset('js/jquery.form.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/contact.js') }}"></script>

    <!-- instafeed-->
    <script type="text/javascript" src="{{ asset('vendors/instafeed/instafeed.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendors/instafeed/script.js')}}"></script>

    <script src="{{ asset('js/theme.js')}}"></script>

    @yield('js')

</body>

</html>
