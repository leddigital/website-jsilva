@extends('layouts.default')
@section('content')

@section('social-tags')

    <meta property="og:title" content="{{ $midia->title }} - JSilva Outdoor">
    <meta property="og:description" content="{{ $informations->meta_description }}">
    <meta property="og:image" content="{{ asset('content/'.$midia->id.'/gallery/'.$single_image) }}">
    <meta property="og:image:alt" content="{{ asset('images/logo.png') }}">

    <meta property="og:image:width" content="800"/>
    <meta property="og:image:height" content="440"/>
    <meta property="og:url" content="{{ route('nav.midia', [$midia->url]) }}">

    <meta name="twitter:title" content="{{ $midia->title }} - JSilva Outdoor">
    <meta name="twitter:description" content="{{ $informations->meta_description }}">
    <meta name="twitter:image"content="{{ asset('content/'.$midia->id.'/gallery/'.$single_image) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>{{ $midia->title }}</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.midias') }}">Mídias</a></li>
                <li><a href="{{ route('nav.midia', ['midia' => $midia->url]) }}">{{ $midia->title }}</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->



<!--================Search Room Area =================-->
<section class="room_details_area">
        <div class="container">
            <div class="row room_details_inner">
                <div class="col-md-8">
                    <div class="room_details_content">
                        <div class="room_d_main_text">
                            <div class="room_details_img owl-carousel">

                                @foreach ($midia->images as $image)
                                    <div class="item">
                                        <img src="{{ asset('content/'.$midia->id.'/gallery/'.$image->image) }}">
                                    </div>
                                @endforeach

                            </div>
                                <h4>{{ $midia->title }}</h4>
                                {!! $midia->content !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="search_right_sidebar">
                        <aside>
                            <div class="r_widget_title">
                                <h3>Conheça outras mídias</h3>
                            </div>
                            <div class="resot_list">
                                <ul>
                                @foreach ($midias as $item)
                                    @if ($midia->url != $item->url)
                                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{ route('nav.midia', [$item->url]) }}">{{ $item->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Search Room Area =================-->










@endsection
