@extends('layouts.default')
@section('content')

@section('social-tags')

    <meta property="og:title" content="JSilva Outdoor - Líder em mídias O.O.H">
    <meta property="og:description" content="{{ $informations->meta_description }}">
    <meta property="og:image" content="{{ asset('/mainbanner/'.$informations->main_banner) }}">
    <meta property="og:image:alt" content="{{ asset('images/logo.png') }}">

    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <meta property="og:url" content="{{ route('nav.about') }}">

    <meta name="twitter:title" content="JSilva Outdoor - Líder em mídias O.O.H">
    <meta name="twitter:description" content="{{ $informations->meta_description }}">
    <meta name="twitter:image"content="{{ asset('/mainbanner/'.$informations->main_banner) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>A J.Silva</h3>
            <ul>
                    <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.about') }}">Sobre</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================Resort Story Area =================-->
<section class="introduction_area resort_story_area">
    <div class="container">
        <div class="row introduction_inner">
            <div class="col-md-5">
                <a href="about-us.html#" class="introduction_img">
                    <img src="{{ asset('content/'.$content->id.'/'.$content->image) }}" alt="">
                </a>
            </div>
            <div class="col-md-7">
                <div class="introduction_left_text">
                    <div class="resort_title">
                        <h2><span>{{ $content->title }}</span></h2>
                        <h5>{{ $content->short_description }}</h5>
                    </div>

                    <div style="margin-top:5rem">
                       {!! $content->content !!}
                    </div>

                    <a class="about_btn_b" href="{{ route('nav.contact') }}">Entre em contato</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Resort Story Area =================-->

<!--================Choose Resot Area =================-->

{{-- <section class="choose_resot_area">
    <div class="container">
        <div class="center_title">
            <h2>Nossos <span>Valores</span></h2>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum</p>
        </div>
        <div class="row choose_resot_inner">
            <div class="col-md-5">
                <div class="resot_list">
                    <ul>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Wellness & poll</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Free wifi</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Bar & garden with terrace</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Delicious breakfast</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>HIgh customer satisfaction</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Good parking & security</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Clean room service</li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i>Discount coupons</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-7">
                <img src="img/resot/resot-1.jpg" alt="">
            </div>
        </div>
    </div>
</section> --}}

<!--================End Choose Resot Area =================-->

@endsection
