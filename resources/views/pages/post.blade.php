@extends('layouts.default')
@section('content')

@section('social-tags')

    <meta property="og:title" content="{{ $post->title }} - JSilva Outdoor">
    <meta property="og:description" content="{{ $post->short_description }}">
    <meta property="og:image" content="{{ asset('content/' . $post->id . "/" . $post->image) }}">
    <meta property="og:image:alt" content="{{ asset('images/logo.png') }}">

    <meta property="og:image:width" content="840"/>
    <meta property="og:image:height" content="360"/>
    <meta property="og:url" content="{{ route('nav.post', ['url' => $post->url]) }}">

    <meta name="twitter:title" content="{{ $post->title }} - JSilva Outdoor">
    <meta name="twitter:description" content="{{ $post->short_description }}">
    <meta name="twitter:image"content="{{ asset('content/' . $post->id . "/" . $post->image) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>Blog</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.blog') }}">Blog</a></li>
                <li><a href="{{ route('nav.post', ['url' => $post->url]) }}">{{ $post->title }}</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================Blog Details Area =================-->
<section class="blog_details_area">
    <div class="container">
        <div class="row">
            <div class="col-md-9 pull-right">
                <div class="blog_details_inner">
                    <div class="blog_item">
                        <a href="{{ route('nav.post', ['url' => $post->url]) }}" class="blog_img">
                            <img src="{{ asset('content/' . $post->id . "/" . $post->image) }}" alt="">
                        </a>
                        <div class="blog_text">
                            <a href="{{ route('nav.post', ['url' => $post->url]) }}"><h4>{{ $post->title }}</h4></a>
                            <ul>
                                <li><a href="{{ route('nav.post', ['url' => $post->url]) }}">{{ $post->created_at->format('d/m/Y') }}</a></li>
                            </ul>
                            <article>
                                {!! $post->content !!}
                            </article>

                            {{-- <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartilhar</a></div> --}}

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-right">
                <div class="sidebar_area">
                    <aside class="r_widget search_widget">
                        <div class="input-group">
                            <form action="{{ route('nav.search') }}" method="GET">
                                <span class="input-group-btn">
                                    <input name="search" type="text" class="form-control" placeholder="O que você procura?" required>
                                    <button class="btn btn-default" type="submit"><i class="icon icon-Search"></i></button>
                                </span>
                            </form>
                        </div><!-- /input-group -->
                    </aside>
                    <aside class="r_widget recent_widget">
                        <div class="r_widget_title">
                            <h3>Últimas notícias</h3>
                        </div>
                        <div class="recent_inner">

                            @foreach ($latests as $latest)
                                <div class="recent_item">
                                    <a href="{{ route('nav.index') . '/blog/' . $latest->url }}"><h4>{{ $latest->title }}</h4></a>
                                    <h5>{{ $latest->created_at->format('d/m/Y') }}</h5>
                                </div>
                            @endforeach

                        </div>
                    </aside>
                    <aside>
                        <div class="r_widget_title">
                            <h3>Nossas mídias</h3>
                        </div>
                        <div class="resot_list">
                            <ul>
                            @foreach ($midias as $item)
                                <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{ route('nav.midia', [$item->url]) }}">{{ $item->title }}</a></li>
                            @endforeach
                        </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Blog Details Area =================-->

@endsection
