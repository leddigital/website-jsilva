@extends('layouts.default')
@section('content')

@section('social-tags')

    <meta property="og:title" content="JSilva Outdoor - Líder em mídias O.O.H">
    <meta property="og:description" content="{{ $informations->meta_description }}">
    <meta property="og:image" content="{{ asset('/mainbanner/'.$informations->main_banner) }}">
    <meta property="og:image:alt" content="{{ asset('images/logo.png') }}">

    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <meta property="og:url" content="{{ route('nav.index') }}">

    <meta name="twitter:title" content="JSilva Outdoor - Líder em mídias O.O.H">
    <meta name="twitter:description" content="{{ $informations->meta_description }}">
    <meta name="twitter:image"content="{{ asset('/mainbanner/'.$informations->main_banner) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection



<section class="main_slider_area">
    <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
        <ul>
            @foreach ($banners as $banner)

            <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                data-thumb="0" data-rotate="0" data-saveperformance="off"
                data-title="{{ $banner->title }}" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5=""
                data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                <!-- MAIN IMAGE -->
                <img src="{{ asset('banners/'.$banner->image) }}" alt="" data-bgposition="center" data-bgfit="cover"
                    data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                <!-- LAYER NR. 1 -->
                <div class="slider_text_box">
                    <div class="tp-caption tp-resizeme first_text" id="slide-1586-layer-1"
                        data-x="['left','left','left','15','0']" data-hoffset="['50','0','0','0']"
                        data-y="['top','top','top','top']" data-voffset="['290','290','290','220','130']"
                        data-fontsize="['55','55','55','40','25']" data-lineheight="['59','59','59','50','35']"
                        data-width="['550','550','550','550','300']" data-height="none" data-whitespace="normal"
                        data-type="text" data-responsive_offset="on"
                        data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                        data-textAlign="['left','left','left','left']">{!! $banner->info1 !!}
                    </div>

                    <div class="tp-caption tp-resizeme secand_text" id="slide-1587-layer-2"
                        data-x="['left','left','left','15','0']" data-hoffset="['50','0','0','0']"
                        data-y="['top','top','top','top']" data-voffset="['500','500','500','500','225']"
                        data-fontsize="['18','18','18','18','16']" data-lineheight="['26','26','26','26']"
                        data-width="['550','550','550','550','300']" data-height="none" data-whitespace="normal"
                        data-type="text" data-responsive_offset="on" data-transform_idle="o:1;"
                        data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                        {!! $banner->info2 !!}
                    </div>
                </div>
            </li>

            @endforeach
        </ul>
    </div>
</section>
<!--================End Slider Area =================-->




<!--================Our Service Area =================-->
<section class="our_service_area">
    <div class="container">
        <div class="row our_service_inner">
            <div class="col-md-3 col-sm-6">
                <div class="our_service_first">
                    <h3>Nossos produtos</h3>
                    <p>O Grupo J.Silva é lider no segmento Out-Of-Home no interior Paulista, com atuação
                        nas áreas de Mobilário Urbano e Grandes Formatos, como:  Outdoors | Painéis Rodoviários |
                        Totens | Áticos de Edifício | Empenas | Relógios<br />
                        Com a J.Silva é mais atenção em todos os pontos.
                    </p>
                    <a class="all_s_btn" href="{{ route('nav.midias') }}">ver todos os produtos</a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{ route('nav.midia', ['outdoor']) }}">
                    <div class="our_service_item">
                        <div class="our_service_item_img">
                            <img src="{{ asset('images/icn-out.png') }}" alt="Outdoor">
                        </div>
                        <h4>Outdoor</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{ route('nav.midia', ['painel-rodoviario']) }}">
                    <div class="our_service_item">
                        <div class="our_service_item_img">
                            <img src="{{ asset('images/icn-painel.png') }}" alt="Painel Rodoviário">
                        </div>
                        <h4>Painel Rodoviário</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{ route('nav.midia', ['frontlight']) }}">
                    <div class="our_service_item">
                        <div class="our_service_item_img">
                            <img src="{{ asset('images/icn-front.png') }}" alt="Painel Rodoviário">
                        </div>
                        <h4>Frontlight</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{ route('nav.midia', ['empena']) }}">
                    <div class="our_service_item">
                        <div class="our_service_item_img">
                            <img src="{{ asset('images/icn-empena.png') }}" alt="Painel Rodoviário">
                        </div>
                        <h4>Empena</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{ route('nav.midia', ['relogio']) }}">
                    <div class="our_service_item">
                        <div class="our_service_item_img">
                            <img src="{{ asset('images/icn-relogio.png') }}" alt="Painel Rodoviário">
                        </div>
                        <h4>Relógio</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="{{ route('nav.midia', ['atico']) }}">
                    <div class="our_service_item">
                        <div class="our_service_item_img">
                            <img src="{{ asset('images/icn-atico.png') }}" alt="Painel Rodoviário">
                        </div>
                        <h4>Ático</h4>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!--================End Our Service Area =================-->


<!--================Fun Fact Area =================-->
<section class="fun_fact_area">
    <div class="container">
        <div class="row">
            <div class="fun_fact_box row m0">
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="media-left counter-wrapper">
                            <h3 class="counter">{{ $informations->fact_1 }}</h3><h3>{{ $informations->fact_1_icon }}</h3>
                        </div>
                        <div class="media-body">
                            <h4>{{ $informations->fact_1_text }}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="media-left counter-wrapper">
                            <h3 class="counter">{{ $informations->fact_2 }}</h3><h3>{{ $informations->fact_2_icon }}</h3>
                        </div>
                        <div class="media-body">
                            <h4>{{ $informations->fact_2_text }}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="media-left counter-wrapper">
                            <h3 class="counter">{{ $informations->fact_3 }}</h3><h3>{{ $informations->fact_3_icon }}</h3>
                        </div>
                        <div class="media-body">
                            <h4>{{ $informations->fact_3_text }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Fun Fact Area =================-->


<!--================Our Resort Gallery Area =================-->
<section class="our_resort_gallery_area">
    <div class="middle_title">
        <h2>Soluções <span>criativas</span></h2>
        <p>Dê forma à sua ideia. Seja inovador(a). Evidencie o que há de mais criativo. <br />
Apliques, efeitos visuais e sonoros são boas maneiras de expressar grandes ideias e impactar seu público.
</p>
    </div>
</section>
<div class="resort_gallery_inner imageGallery1">
    <div class="resort_gallery owl-carousel">
        @foreach ($projetos as $projeto)
        <div class="item">
            <img class="img-responsive" src="{{ asset('content/'.$projeto->id."/".$projeto->image) }}" alt="{{ $projeto->title }}">
            <div class="resort_g_hover">
                <div class="resort_hover_inner">
                    <a class="light" title="{{ $projeto->short_description }}" href="{{ asset('content/'.$projeto->id."/".$projeto->image) }}">
                        <i class="fa fa-expand" aria-hidden="true"></i>
                    </a>
                    <h5>{{ $projeto->title }}</h5>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<!--================End Our Resort Gallery Area =================-->



@if ($news->count() != 0)

    <!--================Latest News Area =================-->

    <section class="latest_news_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row latest_news_left">
                        <div class="left_ex_title">
                            <h2>Últimas <span>Notícias</span></h2>
                        </div>
                        @foreach ($news as $new)
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="l_news_item">
                                        <a href="{{ route('nav.post', ['url' => $new->url]) }}" class="news_img">
                                            <img class="img-responsive" src="{{ 'content/' . $new->id . "/" . $new->image }}" alt="">
                                        </a>
                                        <div class="news_text">
                                            <a class="l_date" href="{{ route('nav.post', ['url' => $new->url]) }}">{{ $new->created_at->format('d/m/Y') }}</a>
                                            <a href="{{ route('nav.post', ['url' => $new->url]) }}">
                                                <h4>{{ $new->title }}</h4>
                                            </a>
                                            <p>{{ str_limit($new->short_description, $limit = 85, $end = '...') }}</p>
                                            <a class="news_more" href="{{ route('nav.post', ['url' => $new->url]) }}">Leia mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--================End Latest News Area =================-->



@endif



@endsection
