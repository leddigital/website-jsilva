@extends('layouts.default')
@section('content')

@section('social-tags')

    <meta property="og:title" content="Contato - JSilva Outdoor">
    <meta property="og:description" content="{{ $informations->meta_description }}">
    <meta property="og:image" content="{{ asset('/mainbanner/'.$informations->main_banner) }}">
    <meta property="og:image:alt" content="{{ asset('images/logo.png') }}">

    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <meta property="og:url" content="{{ route('nav.contact') }}">

    <meta name="twitter:title" content="Contato - JSilva Outdoor">
    <meta name="twitter:description" content="{{ $informations->meta_description }}">
    <meta name="twitter:image"content="{{ asset('/mainbanner/'.$informations->main_banner) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection


<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>Blog</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.contact') }}">Contato</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================Get Contact Area =================-->
<section class="get_contact_area">
    <div class="container">
        <div class="row get_contact_inner">
            <div class="col-md-6">
                <div class="left_ex_title">
                    <h2>Entre em <span>contato</span></h2>
                </div>
                <form action="{{ route('send.mail') }}" class="contact_us_form row m0" method="post" id="contactForm" novalidate="novalidate">
                    @csrf
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nome">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control phonemask" id="number" name="number" placeholder="Telefone">
                    </div>
                    <div class="form-group col-md-12">
                        <textarea class="form-control" name="text" id="message" rows="1" placeholder="Mensagem"></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" value="submit" class="btn submit_btn form-control">Enviar</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <div class="right_contact_info">
                    <div class="contact_info_title">
                        <h3>informações de contato</h3>
                        <p>Tem alguma dúvida, crítica ou sugestão? Entre em contato com a gente.</p>
                    </div>
                    <div class="contact_info_list">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="media-body">

                                    <h4>J. Silva Painéis</h4>
                                    <p>{{ $informations->address1 }}, {{ $informations->number1 }}, {{ $informations->district1 }} <br /> {{ $informations->city1 }} - {{ $informations->state1 }}, CEP {{ $informations->zipcode1 }}</p>
                                    <i class="fa fa-envelope-o"></i><a href="mailto:{{ $informations->email1 }}">{{ $informations->email1 }}</a> <br>
                                    <i class="fa fa-phone"></i><a href="tel:{{ $informations->phone1 }}">{{ $informations->phone1 }}</a>

                            </div>
                        </div>

                        <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="media-body">

                                        <h4>J. Silva Outdoor</h4>
                                        <p>{{ $informations->address2 }}, {{ $informations->number2 }}, {{ $informations->district2 }} <br /> {{ $informations->city2 }} - {{ $informations->state2 }}, CEP {{ $informations->zipcode2 }}</p>
                                        <i class="fa fa-envelope-o"></i><a href="mailto:{{ $informations->email2 }}">{{ $informations->email2 }}</a> <br>
                                        <i class="fa fa-phone"></i><a href="tel:{{ $informations->phone2 }}">{{ $informations->phone2 }}</a>

                                </div>
                            </div>
                        {{-- <div class="media">
                            <div class="media-left">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="media-body">
                                <h4>E-mails</h4>


                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="media-body">
                                <h4>Telefones</h4>


                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--================Contact Success and Error message Area =================-->
<div id="success" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                <h2 class="modal-title">Agradecemos seu contato</h2>
                <p class="modal-subtitle">Sua mensagem foi recebida. Entraremos em contato com você em breve pelo telefone ou e-mail informados</p>
            </div>
        </div>
    </div>
</div>

<!-- Modals error -->

<div id="error" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                <h2 class="modal-title">Ops, algo deu errado</h2>
                <p class="modal-subtitle"> Por favor tente novamente mais tarde </p>
            </div>
        </div>
    </div>
</div>
<!--================End Contact Success and Error message Area =================-->



<!--================End Get Contact Area =================-->

<!--================Map Area =================-->
<section class="contact_map_area">
    <div class="container">
        <div class="row">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3729.654500419514!2d-49.37471328569133!3d-20.805260986119848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94bdad4e4ccef475%3A0xb32f44d8ac54129a!2sJ%20Silva%20Outdoor!5e0!3m2!1spt-BR!2sbr!4v1572028266130!5m2!1spt-BR!2sbr"
            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</section>
<!--================End Map Area =================-->

@endsection
