@extends('layouts.admin')
@section('title', 'Informações do Site')
@section('content')
<header class="page-header">
    <h2>Informações Básicas</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Informações Básicas</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action={{ route('information.save') }}
            data-reload="{{ route('information.index') }}">
            @csrf
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Redes Sociais e Contato</h2>
                    <p class="card-subtitle">
                        Informações de redes sociais e contato.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-facebook"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->facebook)?$entity->facebook:'' }}" type="text" name="facebook"
                                            class="form-control" placeholder="facebook">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-instagram"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->instagram)?$entity->instagram:'' }}" type="text" name="instagram"
                                            class="form-control" placeholder="instagram">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-linkedin"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->linkedin)?$entity->linkedin:'' }}" name="linkedin"
                                            class="form-control" placeholder="linkedin">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-twitter"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->twitter)?$entity->twitter:'' }}" name="twitter"
                                            class="form-control" placeholder="twitter">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="email1" value="{{ isset($entity->email1)?$entity->email1:'' }}"
                                            class="form-control" placeholder="E-mail 1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="email2" value="{{ isset($entity->email2)?$entity->email2:'' }}"
                                            class="form-control" placeholder="E-mail 2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fab fa-whatsapp"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="whatsapp" value="{{ isset($entity->whatsapp)?$entity->whatsapp:'' }}"
                                            class="form-control" placeholder="whatsapp">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="phone1" value="{{ isset($entity->phone1)?$entity->phone1:'' }}"
                                            class="form-control" placeholder="telefone 1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </span>
                                        <input type="text" name="phone2" value="{{ isset($entity->phone2)?$entity->phone2:'' }}"
                                            class="form-control" placeholder="telefone 2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Endereço 1</h2>
                    <p class="card-subtitle">
                        Informações de endereço da empresa que irão aparecer no site.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-map-marker-alt"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->address1)?$entity->address1:'' }}" name="address1"
                                            class="form-control" placeholder="Endereço">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->number1)?$entity->number1:'' }}" name="number1"
                                            class="form-control" placeholder="nº">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->city1)?$entity->city1:'' }}" name="city1"
                                            class="form-control" placeholder="Cidade">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->district1)?$entity->district1:'' }}" name="district1"
                                            class="form-control" placeholder="Bairro">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->state1)?$entity->state1:'' }}" name="state1"
                                            class="form-control" placeholder="Estado">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->zipcode1)?$entity->zipcode1:'' }}" name="zipcode1" class="form-control"
                                            placeholder="CEP">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>


            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Endereço 2</h2>
                    <p class="card-subtitle">
                        Informações de endereço da empresa que irão aparecer no site.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-map-marker-alt"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ isset($entity->address2)?$entity->address2:'' }}" name="address2"
                                            class="form-control" placeholder="Endereço">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->number2)?$entity->number2:'' }}" name="number2"
                                            class="form-control" placeholder="nº">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->city2)?$entity->city2:'' }}" name="city2"
                                            class="form-control" placeholder="Cidade">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->district2)?$entity->district2:'' }}" name="district2"
                                            class="form-control" placeholder="Bairro">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->state2)?$entity->state2:'' }}" name="state2"
                                            class="form-control" placeholder="Estado">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <input type="text" value="{{ isset($entity->zipcode2)?$entity->zipcode2:'' }}" name="zipcode2" class="form-control"
                                            placeholder="CEP">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Meta descrição</h2>
                    <p class="card-subtitle">
                        Texto que será exibido na página de pesquisa dos motores de buscas
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <textarea name="meta_description" class="form-control" row="3">{{ isset($entity->meta_description)?$entity->meta_description:'' }}</textarea>
                            <div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Números</h2>
                    <p class="card-subtitle">
                        Indique os números e textos que irão aparecer na página inicial
                    </p>
                </header>
                <div class="card-body">
                    <div class="row">
                       <div class="col-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-8">
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    value="{{ isset($entity->fact_1)?$entity->fact_1:"" }}" name="fact_1" placeholder="Número 1">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text"
                                                value="{{ isset($entity->fact_1_icon)?$entity->fact_1_icon:"" }}" name="fact_1_icon" placeholder="Icone">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-12">
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    value="{{ isset($entity->fact_1_text)?$entity->fact_1_text:"" }}" name="fact_1_text" placeholder="Texto 1">
                                            </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                        <div class="col-4">
                                <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-8">
                                                    <div class="form-group">
                                                        <input class="form-control" type="text"
                                                            value="{{ isset($entity->fact_2)?$entity->fact_2:"" }}" name="fact_2" placeholder="Número 1">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="text"
                                                        value="{{ isset($entity->fact_2_icon)?$entity->fact_2_icon:"" }}" name="fact_2_icon" placeholder="Icone">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-12">
                                                    <div class="form-group">
                                                        <input class="form-control" type="text"
                                                            value="{{ isset($entity->fact_2_text)?$entity->fact_2_text:"" }}" name="fact_2_text" placeholder="Texto 2">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                       <div class="col-4">
                            <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-8">
                                                <div class="form-group">
                                                    <input class="form-control" type="text"
                                                        value="{{ isset($entity->fact_3)?$entity->fact_3:"" }}" name="fact_3" placeholder="Número 3">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    value="{{ isset($entity->fact_3_icon)?$entity->fact_3_icon:"" }}" name="fact_3_icon" placeholder="Icone">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-12">
                                                <div class="form-group">
                                                    <input class="form-control" type="text"
                                                        value="{{ isset($entity->fact_3_text)?$entity->fact_3_text:"" }}" name="fact_3_text" placeholder="Texto 3">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success">Salvar</button>
                </div>

            </section>

        </form>
    </div>
</div>
@endsection
