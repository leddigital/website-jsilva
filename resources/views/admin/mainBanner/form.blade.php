@extends('layouts.admin')
@section('title', 'Banners')
@section('content')
<header class="page-header">
    <h2>Banners</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Cadastro de Banners</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ route('mainbanner.save') }}"
            data-reload="{{ route('mainbanner.index') }}">
            @csrf

            <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                        </div>
                        <h2 class="card-title">Banner Principal</h2>
                        <p class="card-subtitle">
                            Selecione o banner que irá aparecer nas páginas internas do site
                        </p>
                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <small>Tamanho da imagem 2000px x 840</small>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Trocar</span>
                                            <span class="fileupload-new">Selecionar Imagem</span>
                                            <input type="file" name="main_banner" accept="image/*"
                                                onchange='loadPreview(this, 2000,840)' {{ isset($entity)?'':'required' }}>
                                        </span>
                                        <a href="forms-basic.html#" class="btn btn-default fileupload-exists"
                                            data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="img-content">
                                    <img id='output' class='img-fluid' src='{{ isset($entity->main_banner)!=""?"/mainbanner/".$entity->main_banner:'' }}'>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>
        </form>
    </div>
</div>
@endsection
